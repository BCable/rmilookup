import java.math.BigInteger;
import java.nio.charset.Charset;
import java.rmi.*;

public class RMILookUp {
	private static void dumpHex(String out){
		System.out.println(out);
		System.out.println("Hex Output:");
		System.out.println(
			String.format("%040x",
				new BigInteger(1, out.getBytes(Charset.forName("UTF-8")))
			)
		);
	}

	public static void main(String args[]){
		// Some sort of securitymanage thinger
		if (System.getSecurityManager() == null){
			System.setSecurityManager(new SecurityManager());
		}

		try {
			RMIPrint remote;
			String lookupName = args[0];

			// Query Remote Server
			remote = (RMIPrint) Naming.lookup(lookupName);
			String returnValue = remote.print();

			// Dump Output, with hex dump
			dumpHex(returnValue);

		// Catch Exceptions
		} catch (Exception e) {
			System.err.println("RMILookUp exception:" );
			String stackTrace = e.toString();

			// Dump Output, with hex dump
			dumpHex(stackTrace);
		}
	}
}
