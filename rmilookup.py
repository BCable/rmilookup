#!/usr/bin/env python3

import re, socket, sys
from binascii import hexlify, unhexlify

# parse RMI url
rmi_url = sys.argv[1]
rmi_match = re.match("^rmi://([^:]+)(:([0-9]+))?([/](.*))?$", rmi_url)
rmi_groups = rmi_match.groups()
rmi_ip = rmi_groups[0]
rmi_port = int(rmi_groups[2])
rmi_object = rmi_groups[4]
if rmi_port is None:
	rmi_port = 1099
if rmi_object is None:
	rmi_object = b""

print("RMI_IP: {}".format(rmi_ip))
print("RMI_PORT: {}".format(rmi_port))
print("RMI_OBJECT: {}".format(rmi_object))

# initialize connection
sock = socket.socket(
	socket.AF_INET, socket.SOCK_STREAM
)
sock.connect((rmi_ip, rmi_port))

# initial header
sock.sendall(b"""\x4a\x52\x4d\x49\x00\x02\x4b""")

# receive protocol status
protocol_status_b, addr = sock.recvfrom(1)
print("RECV_HEADER_INFO: {}".format(hexlify(protocol_status_b)))

# receive length of hostname
host_length_b, addr = sock.recvfrom(2)
host_length = int(hexlify(host_length_b).decode(), 16)
print("RECV_HOST_LENGTH: {}".format(host_length_b))
print("DECODE_HOST_LENGTH: {}".format(host_length))

# read hostname
host_name_b, addr = sock.recvfrom(host_length)
print("RECV_HOST_NAME: {}".format(host_name_b))
print("DECODE_HOST_NAME: {}".format(host_name_b.decode()))

# read final head
final_head_b, addr = sock.recvfrom(4)
print("RECV_FINAL_HEAD: {}".format(hexlify(final_head_b)))

# construct new packet to send back
new_packet_b = host_length_b + host_name_b + b"\x00"*4 + \
	b"\x50\xac\xed\x00\x05\x77" + b"\x00"*25 + \
	b"\x02\x44\x15\x4d\xc9\xd4\xe6\x3b\xdf\x74"

new_packet_b += unhexlify("{:0>4x}".format(len(rmi_object)))
new_packet_b += rmi_object.encode()
print("NEW_PACKET_B: {}".format(new_packet_b))
print("NEW_PACKET_HEX: {}".format(hexlify(new_packet_b)))

# send new packet
sock.sendall(new_packet_b)

# recv final payload from server
msg, addr = sock.recvfrom(4096)
print("RESPONSE: {}".format(msg))
print("RESPONSE_HEX: {}".format(hexlify(msg)))
