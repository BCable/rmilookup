import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RMITestServer extends RMIPrintImpl {
	public RMITestServer() {}
	public static void main(String args[]) {
		try {
			RMIPrintImpl obj = new RMIPrintImpl(); 
			RMIPrint print_obj = (RMIPrint) UnicastRemoteObject.exportObject(obj, 0); 
			Registry registry = LocateRegistry.getRegistry(); 
			registry.bind("RMIPrint", print_obj);
			System.err.println("Server ready");

		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
