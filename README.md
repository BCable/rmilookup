rmilookup
=========

Simple RMI lookup Java and Python client for preparation for potential onslaught of Log4J research.

The Java client pretends to be a vulnerable Log4J install to accept a payload from an attacker, but requires you to Wireshark the payload.  This allows reverse engineering and helps in debugging the slightly but mostly completed Python3 rmilookup.py that should be standalone quick lookup for these links when completed.

https://bcable.net/analysis-httpd-log4j_obfuscate.html

https://www.bleepingcomputer.com/news/security/log4j-attackers-switch-to-injecting-monero-miners-via-rmi/

Looks like they are using these examples:

https://www.veracode.com/blog/research/exploiting-jndi-injections-java

## Compilation // Requirements

Requires a working JDK to compile the Java version (for Wireshark sniffing of RMI).  For Python requires Python 3.

## Testing RMI

Launching a server, using `rmiregistry` (or any RMI server, really), this should allow for a test of the querying.

```
$ bash rmilookup.sh rmi://127.0.0.1:1099/RMIPrint
RMILookUp exception:
java.rmi.NotBoundException: RMIPrint
Hex Output:
6a6176612e726d692e4e6f74426f756e64457863657074696f6e3a20524d495072696e74
```

For the Python3 final version, which hasn't been fully tested (I need to find more JNDI RMI exploits to connect into), this should work:

```
$ python3 rmilookup.py rmi://ip.add.re.ss:port/object
```
