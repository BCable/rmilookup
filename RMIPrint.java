import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIPrint extends Remote {
	public String print() throws RemoteException;
}
