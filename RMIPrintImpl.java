import java.rmi.Remote;
import java.rmi.RemoteException;

public class RMIPrintImpl implements RMIPrint {
	public String print() throws RemoteException {
		System.out.println("This is where printed output ends up.");
		return "This is where return output ends up.";
	}
}
